import json
import numpy as np
import torch
import torch.nn as nn
from torch.utils.data import TensorDataset, DataLoader
import torch.nn.functional as F
from models import SimpleNet
from models import TwoLayerNet
from misc_func import avgWeights, firstWight, dim_io
import os
import matplotlib.pyplot as plt
from prettytable import PrettyTable

# device
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
device = torch.device("cpu")

# Load
PATH = os.path.dirname(os.path.realpath(__file__))+'/save/twoLayerNet/cs2.pth'
#PATH = os.path.dirname(os.path.realpath(__file__))+'/save/twoLayerNet/dc1.pth'
#PATH = os.path.dirname(os.path.realpath(__file__))+'/save/twoLayerNet/ou1.5.pth'

checkpoint = torch.load(PATH)
input_dimensions = checkpoint['iho'][0]
hidden_dimensions = checkpoint['iho'][1]
output_dimensions = checkpoint['iho'][2]
model = TwoLayerNet(input_dimensions, hidden_dimensions, output_dimensions)
model.load_state_dict(checkpoint['model_state_dict'])
model.eval()
optimizer = torch.optim.SGD(model.parameters(), 1e-5)
optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
epoch = checkpoint['epoch']
loss = checkpoint['loss']

filename = checkpoint['train_file']
with open(filename) as json_file:  
    data = json.load(json_file)

# print
print(data["dictionary"])
print("Trainset :", data["info"]["count_in/out"])
print("device : ", device)
print("input_dimensions : ", input_dimensions)
print("hidden_dimensions : ", hidden_dimensions)
print("output_dimensions : ", output_dimensions)
print("epoch : ", epoch)
print("loss : ", loss)

print(model)
# print

marketOffset = 14
market_id = next(iter(data["dictionary"]["markets_out"]), None)

outputmarket = data["dictionary"]["markets_out"][market_id]["outputs"][0:marketOffset]
outputmarket.append(" - ")


for k, ar in enumerate(data["test_in"]):
    test = np.array(ar, dtype='float32')
    test = torch.from_numpy(test)
    prediction = model(test)
    prediction = prediction.detach().numpy()
    print("------------------------------------------------------------------------------")
    x = PrettyTable()
    x.field_names = ["1", "x", "2", "over", "under", "ou-line", "home", "away", "ha-line"]
    x.add_row(ar)
    print(x)

    o = PrettyTable()
    o.field_names = outputmarket
    o.add_row(np.append(prediction[0:marketOffset], "prediction"))
    o.add_row(np.append(data["test_out"][k][0:marketOffset], "real"))

    print(o)
    print("------------------------------------------------------------------------------")