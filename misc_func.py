# Python program to get average of a list 
def Average(lst): 
    return sum(lst) / len(lst) 

def avgWeights(tensor):
    avglist = list()
    for x in tensor:
        newlist = list()
        for y in x:
            newlist.append(y)
        avglist.append(Average(newlist))
    return Average(avglist)

def firstWight(tensor):
    for x in tensor:
        for y in x:
            return y

def dim_io(array, check):
    dimensions = 0

    for i, market in array.items():
        if check is not False:
            if market["Line"] is not False:
                dimensions = dimensions + 1
        dimensions = dimensions + len(market["outputs"])
    return dimensions