import json
import numpy as np
import torch
import torch.nn as nn
from torch.utils.data import TensorDataset, DataLoader
import torch.nn.functional as F
from models import SimpleNet
from models import TwoLayerNet
from misc_func import avgWeights, firstWight, dim_io
import os
import matplotlib.pyplot as plt

# device
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
device = torch.device("cpu")

# config & train data
filename = 'train_data/[1X2][OU-2.5][AH-O]_to_[CS][set1].json'
filename = 'train_data/[1X2][OU-2.5][AH-O]_to_[OU-1.5][set1].json'
with open(filename) as json_file:  
    data = json.load(json_file)

input_dimensions = (dim_io(data["dictionary"]["markets_in"], True))
output_dimensions = (dim_io(data["dictionary"]["markets_out"], False))
hidden_dimensions = 900
batch_size = 12
# config & train data

# Inputs
inputs = np.array(data["input"], dtype='float32')
targets = np.array(data["output"], dtype='float32')
inputs = torch.from_numpy(inputs).to(device)
targets = torch.from_numpy(targets).to(device)
train_ds = TensorDataset(inputs, targets)
train_dl = DataLoader(train_ds, batch_size, shuffle=True)
# Model
model = TwoLayerNet(input_dimensions, hidden_dimensions, output_dimensions).to(device)
opt = torch.optim.SGD(model.parameters(), 0.000326)
loss_fn = F.mse_loss

# print
print(data["dictionary"])
print("Trainset :", data["info"]["count_in/out"])
print("device : ", device)
print("input_dimensions : ", input_dimensions)
print("hidden_dimensions : ", hidden_dimensions)
print("output_dimensions : ", output_dimensions)
print("batch_size : ", batch_size)
print(model)
# print

# model save
SAVE_PATH = os.path.dirname(os.path.realpath(__file__))+'/save/twoLayerNet/ou1.5.pth'

# Load
# checkpoint = torch.load(SAVE_PATH)
# model.load_state_dict(checkpoint['model_state_dict'])
# opt.load_state_dict(checkpoint['optimizer_state_dict'])
# model.eval()
# epoch_num = checkpoint['epoch']
# loss = checkpoint['loss']


# train func
epoch_num = 0
mse_list = []
epoch_list = []
def fit(train_for, epoch_num, model, loss_fn, opt):
    for epoch in range(train_for):
        epoch_num += 1
        for xb,yb in train_dl:
            # Generate predictions
            pred = model(xb)
            loss = loss_fn(pred, yb)
            # Perform gradient descent
            loss.backward()
            opt.step()
            opt.zero_grad()
            # debug data
            epoch_list.append(epoch_num)
            mse_list.append(loss_fn(model(inputs), targets).item())
            print('Training loss: ',epoch, loss_fn(model(inputs), targets))            
    return epoch_num

epoch_num = fit(1000, epoch_num, model, loss_fn, opt)

plt.plot(epoch_list, mse_list)
plt.ylabel('mse_loss')
plt.xlabel('epoch')
plt.show()

torch.save({
            'epoch': epoch_num,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': opt.state_dict(),
            'loss': loss_fn(model(inputs), targets).item(),
            'iho' : [input_dimensions, hidden_dimensions, output_dimensions],
            'train_file' : filename
            }, SAVE_PATH)